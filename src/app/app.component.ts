import { Component } from '@angular/core';
import { SeedingAppDataService } from './config/seeding-app-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(private seeder: SeedingAppDataService) {
    this.seeder.seedApp();
  }
}
