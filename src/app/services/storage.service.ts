import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Discount } from '../models/Discount';
import { Item } from '../models/Item';
import { Constants } from '../utils/constants';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private constructor() {
    try {
      console.info('Retrieving data from localStorage');

      const sItems: string = localStorage.getItem(Constants.ITEM_KEY)!;
      const sDiscounts: string = localStorage.getItem(Constants.DISCOUNT_KEY)!;
      if (sItems) {
        this.items.slice();
        this.storeItem(JSON.parse(sItems));
      }

      if (sDiscounts) {
        this.discounts.slice();
        this.storeDiscount(JSON.parse(sDiscounts));
      }
    } catch (e) {
      console.error('Something went wrong...', e);
    }
  }
  // *************************************** //
  // *Discount CRUD*
  // singleton
  private _discounts: Discount[];

  get discounts(): Discount[] {
    if (!this._discounts) {
      this._discounts = [];
    }

    return this._discounts;
  }

  private $discounts: BehaviorSubject<Discount[]> = new BehaviorSubject<
    Discount[]
  >(this.discounts);

  get discounts$(): Observable<Discount[]> {
    return this.$discounts.asObservable();
  }

  storeDiscount(d: Discount | Discount[]): void {
    if (Array.isArray(d)) {
      // values are cloned then stored
      // by the way, this section of the code is only used by Seeding Service
      this.discounts.push(...[...d]);
    } else {
      // Do not push if the discount already exists
      if (
        !this.discounts.some(
          (discount) => discount.itemNameA === discount.itemNameB
        )
      ) {
        // value is cloned then stored
        this.discounts.push({ ...d });
      }
    }

    this.emitDiscount();
  }

  deleteDiscount(currentDiscount: Discount) {
    const index = this.discounts.indexOf(currentDiscount, 0);
    if (index > -1) {
      this.discounts.splice(index, 1);
      this.emitDiscount();
    }
  }

  editDiscount(currentDiscount: Discount, changes: Partial<Discount>): void {
    const index = this.discounts.indexOf(currentDiscount, 0);
    if (index > -1) {
      this.discounts[index] = Object.assign(currentDiscount, changes);
      this.emitDiscount();
    }
  }

  private emitDiscount(): void {
    localStorage.setItem(Constants.DISCOUNT_KEY, JSON.stringify(this.discounts));
    this.$discounts.next(this.discounts);
  }

  // *************************************** //
  // *Item CRUD*
  // singleton
  private _items: Item[];

  get items(): Item[] {
    if (!this._items) {
      this._items = [];
    }

    return this._items;
  }

  private $items: BehaviorSubject<Item[]> = new BehaviorSubject<Item[]>(
    this.items
  );

  get items$(): Observable<Item[]> {
    return this.$items.asObservable();
  }

  storeItem(i: Item | Item[]) {
    if (Array.isArray(i)) {
      // values are cloned then stored
      // by the way, this section of the code is only used by Seeding Service
      this.items.push(...[...i]);
    } else {
      // Do not push if the item already exists
      if (!this.items.some((item) => item.name === i.name)) {
        // value is cloned then stored
        this.items.push({ ...i });
      }
    }
    this.emitItems();
  }

  deleteItem(currentItem: Item) {
    const index = this.items.indexOf(currentItem, 0);
    if (index > -1) {
      this.items.splice(index, 1);
      this.emitItems();
    }
  }

  editItem(currentItem: Item, changes: Partial<Item>): void {
    const index = this.items.indexOf(currentItem, 0);
    if (index > -1) {
      this.items[index] = Object.assign(currentItem, changes);
      this.emitItems();
    }
  }

  private emitItems() {
    localStorage.setItem(Constants.ITEM_KEY, JSON.stringify(this.items));
    this.$items.next(this.items);
  }
}
