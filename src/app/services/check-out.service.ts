import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Bill } from '../models/bill';
import { BillDiscountDetail } from '../models/bill-discount-detail';
import { Discount } from '../models/Discount';
import { Item } from '../models/Item';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class CheckOutService {
  constructor(private st: StorageService) {}

  bill: Bill = {
    items: [],
    discounts: [],
    total: 0,
    discount: 0,
    totalWithDiscount: 0,
  };

  // items to process
  items$: Observable<Item[]>;

  calcBill$(): Observable<Bill> {
    return this.items$.pipe(
      map((items) => {
        this.resetBill();
        let clonedItems: Item[] = Object.assign([], items);

        const discountItemNames = new Set([
          ...this.st.discounts.flatMap(({ itemNameA, itemNameB }) => {
            return [itemNameA, itemNameB];
          }),
        ]);

        const itemsWithoutDiscount = clonedItems.filter(
          ({ name }) => !discountItemNames.has(name)
        );

        if (itemsWithoutDiscount && itemsWithoutDiscount.length) {
          this.addBills(itemsWithoutDiscount);
        }

        let itemsWithDiscount = clonedItems.filter(({ name }) =>
          discountItemNames.has(name)
        );

        // discount logic
        if (itemsWithDiscount && 1 >= itemsWithDiscount.length) {
          // it means there are no items to be paired
          // therefore no discounts to be applied :(
          this.addBills(itemsWithDiscount);
          return this.bill;
        }

        for (let index = 0; index < this.st.discounts.length; index++) {
          const discount = this.st.discounts[index];
          console.log('discount: ', discount);

          if (
            itemsWithDiscount.some(({ name }) => name === discount.itemNameA) &&
            itemsWithDiscount.some(({ name }) => name === discount.itemNameB)
          ) {
            // find discount pairs
            const itemsA = itemsWithDiscount.filter(
              ({ name }) => name === discount.itemNameA
            );
            const itemsB = itemsWithDiscount.filter(
              ({ name }) => name === discount.itemNameB
            );

            const minLength = Math.min(itemsA.length, itemsB.length);
            const maxLength = Math.max(itemsA.length, itemsB.length);

            for (let index = 0; index < maxLength; index++) {
              if (index < minLength) {
                // make pairs
                const itemA = itemsA[index];
                const itemB = itemsB[index];
                // apply discount :)
                this.addBills([itemA, itemB]);
                this.addDiscount(discount, itemA, itemB);
              } else if (minLength === maxLength) {
                // noop
                console.log('noop');
                break;
              } else {
                switch (maxLength) {
                  // items that unfortunately have no discount :(
                  case itemsA.length:
                    itemsA.splice(minLength, maxLength);
                    break;
                  case itemsB.length:
                    itemsB.splice(minLength, maxLength);
                    break;
                }
              }
            }

            // To avoid duplicated data the 'pairedItems' are going
            // to be removed from itemsWithDiscount array
            const pairedItems = [...itemsA, ...itemsB];

            pairedItems.forEach((f) =>
              itemsWithDiscount.splice(
                itemsWithDiscount.findIndex((e) => e.name === f.name),
                1
              )
            );

            console.log('leftover itemsWithDiscount: ', itemsWithDiscount);
            console.log('items a b: ', itemsA, itemsB);
          }
        }

        if (itemsWithDiscount && itemsWithDiscount.length) {
          // items without discounts pair :(
          this.addBills(itemsWithDiscount);
        }

        return this.bill;
      })
    );
  }

  private calcProductTotalCost(cost: number, tax: number): number {
    return cost + (cost * tax) / 100;
  }

  private calcProductDiscount(discountValue: number, productsValue: number) {
    return (productsValue * discountValue) / 100;
  }

  private addDiscount(d: Discount, prodA: Item, prodB: Item) {
    const originalAmount =
      this.calcProductTotalCost(prodA.cost, prodA.tax) +
      this.calcProductTotalCost(prodB.cost, prodB.tax);

    const discount = this.calcProductDiscount(d.value, originalAmount);

    const details: BillDiscountDetail = {
      items: [prodA, prodB],
      discountDetails: d,
      originalAmount: originalAmount,
      discountAmount: discount,
      totalAmount: originalAmount - discount,
    };

    this.bill.discounts.push(details);
    this.bill.discount += discount;
    this.bill.totalWithDiscount -= discount;
  }

  private addBills(items: Item[]) {
    const sum = items.reduce(
      (acc, { cost, tax }) => (acc += this.calcProductTotalCost(cost, tax)),
      0
    );
    this.bill.items.push(...items);
    this.bill.total += sum;
    this.bill.totalWithDiscount += sum;
  }

  private resetBill() {
    this.bill = {
      items: [],
      discounts: [],
      total: 0,
      discount: 0,
      totalWithDiscount: 0,
    };
  }
}
