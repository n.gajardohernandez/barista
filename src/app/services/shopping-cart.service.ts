import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Item } from '../models/Item';
import { ShoppingCart } from '../models/shopping-cart';
import { Constants } from '../utils/constants';

@Injectable({
  providedIn: 'root',
})
export class ShoppingCartService {
  private constructor() {
    const sItemsInSC: string = localStorage.getItem(Constants.S_C_KEY)!;

    if (sItemsInSC) {
      this.shoppingCart.items.slice();
      this._shoppingCart = JSON.parse(sItemsInSC);
      this.emitShoppingCart();
    }
  }

  // singleton
  private _shoppingCart: ShoppingCart;

  get shoppingCart(): ShoppingCart {
    if (!this._shoppingCart) {
      this._shoppingCart = {
        items: [],
      };
    }

    return this._shoppingCart;
  }

  private $shoppingCart = new BehaviorSubject<ShoppingCart>(this.shoppingCart);

  get shoppingCart$(): Observable<ShoppingCart> {
    return this.$shoppingCart.asObservable();
  }

  emptyCart(): void {
    this.shoppingCart.items = [];
    this.emitShoppingCart();
  }

  addItem(item: Item): void {
    this.shoppingCart.items.push({ ...item });
    this.emitShoppingCart();
  }

  removeItem(item: Item): void {
    const index = this.shoppingCart.items.indexOf(item, 0);
    if (index > -1) {
      this.shoppingCart.items.splice(index, 1);
      this.emitShoppingCart();
    }
  }

  private emitShoppingCart(): void {
    localStorage.setItem(Constants.S_C_KEY, JSON.stringify(this.shoppingCart));
    this.$shoppingCart.next(this.shoppingCart);
  }
}
