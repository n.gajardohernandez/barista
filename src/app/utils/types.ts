export type EditMode = { position?: number; isEditing?: boolean };
