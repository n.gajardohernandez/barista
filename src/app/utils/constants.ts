export class Constants {
  public static numberPattern: RegExp = /^-?(0|[1-9]\d*)?$/;
  public static decimalPattern: RegExp = /^[+-]?([0-9]+\.?[0-9]*|\.[0-9]+)$/;

  //*  Storage service
  public static ITEM_KEY = 'items';
  public static DISCOUNT_KEY = 'discount';

  //* Shopping cart service
  public static S_C_KEY = 'shopping-cart';

  //* check out service
  public static completeOrderMessage = 'your order will be ready soon';
}
