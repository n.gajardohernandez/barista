import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckOutComponent } from './views/checkout/check-out.component';
import { DiscountsComponent } from './views/discounts/discounts.component';
import { HomeComponent } from './views/home/home.component';
import { ItemsComponent } from './views/items/items.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' }, // redirect to `first-component`
  { path: 'home', component: HomeComponent },
  { path: 'items', component: ItemsComponent },
  { path: 'discounts', component: DiscountsComponent },
  { path: 'checkout', component: CheckOutComponent },
]; // sets up routes constant where you define your routes

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
