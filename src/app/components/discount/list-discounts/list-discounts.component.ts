import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Discount } from 'src/app/models/Discount';
import { EditMode } from 'src/app/utils/types';

@Component({
  selector: 'app-list-discounts',
  templateUrl: './list-discounts.component.html',
  styleUrls: ['./list-discounts.component.css'],
})
export class ListDiscountsComponent implements OnInit {
  constructor() {}

  //* inputs
  @Input() displayedColumns = [
    'position',
    'itemNameA',
    'itemNameB',
    'value',
    'action',
  ];
  @Input() inputData$: Observable<Discount[]>;

  //* outputs
  @Output() editDiscount = new EventEmitter<[Discount, Discount]>();
  @Output() deleteDiscount = new EventEmitter<Discount>();

  //* table
  dataSource: MatTableDataSource<Discount>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  editMode: EditMode = {};

  //* form
  itemNameA = new FormControl('', [Validators.required]);
  itemNameB = new FormControl('', [Validators.required]);
  value = new FormControl('', [Validators.required]);

  form = new FormGroup({
    itemNameA: this.itemNameA,
    itemNameB: this.itemNameB,
    value: this.value,
  });

  //* angular methods
  ngOnInit(): void {
    this.inputData$.subscribe((values) => {
      this.dataSource = new MatTableDataSource<Discount>(values);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  //* table methods
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  toggleEdit(position: number, discount: Discount) {
    if (this.editMode.position === position) {
      this.editMode.position = position;
      this.editMode.isEditing = !this.editMode.isEditing;
    } else {
      this.editMode.position = position;
      this.editMode.isEditing = true;
    }

    this.form.patchValue({
      itemNameA: discount.itemNameA,
      itemNameB: discount.itemNameB,
      value: discount.value,
    });
  }

  isEdit(editMode: EditMode, discount: Discount) {
    return (
      editMode?.isEditing &&
      editMode?.position === this.dataSource.data.indexOf(discount)
    );
  }

  //* emmiter methods
  onEditDiscount(currentDiscount: Discount) {
    this.editDiscount.emit([currentDiscount, this.form.value]);
  }

  onDeleteDiscount(discount: Discount) {
    this.deleteDiscount.emit(discount);
  }
}
