import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Discount } from 'src/app/models/Discount';
import { Constants } from 'src/app/utils/constants';

@Component({
  selector: 'app-add-discount',
  templateUrl: './add-discount.component.html',
  styleUrls: ['./add-discount.component.css'],
})
export class AddDiscountComponent implements OnInit {
  constructor(private fb: FormBuilder) {}
  @Output() addDiscount = new EventEmitter<Discount>();

  form = this.fb.group({
    itemNameA: ['', [Validators.required, Validators.minLength(3)]],
    itemNameB: ['', [Validators.required, Validators.minLength(3)]],
    value: [
      '',
      [
        Validators.required,
        Validators.min(0),
        Validators.pattern(Constants.decimalPattern),
      ],
    ],
  });

  ngOnInit(): void {}

  onSubmit() {
    console.log('Submitting');
    console.log(this.form.value);

    this.addDiscount.emit(this.form.value);

    this.form.patchValue({
      itemNameA: '',
      itemNameB: '',
      value: '',
    });
  }
}
