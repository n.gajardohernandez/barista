import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Bill } from 'src/app/models/bill';
import { BillDiscountDetail } from 'src/app/models/bill-discount-detail';

@Component({
  selector: 'app-bill-details',
  templateUrl: './bill-details.component.html',
  styleUrls: ['./bill-details.component.css'],
})
export class BillDetailsComponent implements OnInit {
  //* inputs
  @Input() displayedColumns = [
    // 'position',
    'itemNameA',
    'itemNameB',
    'originalAmount',
    'value',
    'discountAmount',
    'totalAmount',
  ];
  @Input() inputData$: Observable<BillDiscountDetail[]>;

  //* table
  dataSource: MatTableDataSource<BillDiscountDetail>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  //* angular methods
  ngOnInit(): void {
    this.inputData$.subscribe((values) => {
      this.dataSource = new MatTableDataSource<BillDiscountDetail>(values);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  //* table methods
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
