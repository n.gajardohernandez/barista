import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { Output, EventEmitter } from '@angular/core';
import { EditMode } from 'src/app/utils/types';
import { Item } from 'src/app/models/Item';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.css'],
})
export class ListItemsComponent implements OnInit, AfterViewInit {
  constructor() {}

  //* inputs
  @Input() displayedColumns: string[] = [
    'position',
    'name',
    'cost',
    'tax',
    'action',
  ];

  @Input() displayedButtons: string[] = ['edit', 'delete'];
  @Input() inputData$: Observable<Item[]>;

  //* outputs
  @Output() editItem = new EventEmitter<[Item, Item]>();
  @Output() deleteItem = new EventEmitter<Item>();
  @Output() addToShoppingCart = new EventEmitter<Item>();
  @Output() removeFromShoppingCart = new EventEmitter<Item>();

  //* table
  dataSource: MatTableDataSource<Item>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  editMode: EditMode = {};

  //* form
  name = new FormControl('', Validators.required);
  cost = new FormControl('', Validators.required);
  tax = new FormControl('', Validators.required);

  form = new FormGroup({
    name: this.name,
    cost: this.cost,
    tax: this.tax,
  });

  //* angular methods
  ngOnInit(): void {
    this.inputData$.subscribe((values) => {
      this.dataSource = new MatTableDataSource<Item>(values);
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  //* table methods
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  toggleEdit(position: number, item: Item) {
    if (this.editMode.position === position) {
      this.editMode.position = position;
      this.editMode.isEditing = !this.editMode.isEditing;
    } else {
      this.editMode.position = position;
      this.editMode.isEditing = true;
    }

    this.form.patchValue({
      name: item.name,
      cost: item.cost,
      tax: item.tax,
    });
  }

  isEdit(editMode: EditMode, item: Item) {
    return (
      editMode?.isEditing &&
      editMode?.position === this.dataSource.data.indexOf(item)
    );
  }

  //* emmiter methods
  onEditItem(currentItem: Item) {
    this.editItem.emit([currentItem, this.form.value]);
  }

  onDeleteItem(item: Item) {
    this.deleteItem.emit(item);
  }

  onAddToShoppingCart(item: Item) {
    this.addToShoppingCart.emit(item);
  }

  onRemoveFromShoppingCart(item: Item) {
    this.removeFromShoppingCart.emit(item);
  }
}
