import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Item } from 'src/app/models/Item';
import { Constants } from 'src/app/utils/constants';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css'],
})
export class AddItemComponent implements OnInit {
  constructor(private fb: FormBuilder) {}

  @Output() addItem = new EventEmitter<Item>();

  form = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    cost: [
      '',
      [
        Validators.required,
        Validators.pattern(Constants.decimalPattern),
        Validators.min(0),
      ],
    ],
    tax: [
      '',
      [
        Validators.required,
        Validators.pattern(Constants.decimalPattern),
        Validators.min(0),
      ],
    ],
  });

  ngOnInit(): void {}

  onSubmit() {
    this.addItem.emit(this.form.value);

    this.form.patchValue({
      name: '',
      cost: '',
      tax: '',
    });
  }
}
