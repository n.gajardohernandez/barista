import { TestBed } from '@angular/core/testing';

import { SeedingAppDataService } from './seeding-app-data.service';

describe('SeedingAppDataService', () => {
  let service: SeedingAppDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeedingAppDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
