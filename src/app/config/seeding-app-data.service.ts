import { Injectable } from '@angular/core';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root',
})
export class SeedingAppDataService {
  private constructor(private storageService: StorageService) {}

  public seedApp() {
    // Default data is added only if there are no data stored previously
    if (this.storageService.items && !this.storageService.items.length) {
      console.info('Seeding application with default items :)');
      this.seedItems();
    }

    if (
      this.storageService.discounts &&
      !this.storageService.discounts.length
    ) {
      console.info('Seeding application with default discounts :)');
      this.seedDiscounts();
    }

    console.log(this.storageService.discounts);
    console.log(this.storageService.items);
  }

  private seedItems(): void {
    this.storageService.storeItem([
      {
        name: 'bread',
        cost: 10,
        tax: 19,
      },
      {
        name: 'coffee',
        cost: 10,
        tax: 10,
      },
      {
        name: 'juice',
        cost: 3,
        tax: 0,
      },
      {
        name: 'cookie',
        cost: 2,
        tax: 0,
      },
    ]);
  }

  private seedDiscounts(): void {
    this.storageService.storeDiscount([
      {
        itemNameA: 'bread',
        itemNameB: 'coffee',
        value: 15,
      },
      {
        itemNameA: 'bread',
        itemNameB: 'juice',
        value: 10,
      },
      {
        itemNameA: 'coffee',
        itemNameB: 'juice',
        value: 20,
      },
    ]);
  }
}
