import { BillDiscountDetail } from './bill-discount-detail';
import { Item } from './Item';

export class Bill {
  items: Item[];
  discounts: BillDiscountDetail[];
  total: number;
  discount: number;
  totalWithDiscount: number;
}
