import { Item } from './Item';

export interface ShoppingCart {
  items: Item[];
}
