export class Item {
  name: string;
  cost: number;
  tax: number;
}
