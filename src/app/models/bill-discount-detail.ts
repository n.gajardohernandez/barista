import { Discount } from './Discount';
import { Item } from './Item';

export class BillDiscountDetail {
  items: [Partial<Item>, Partial<Item>];
  discountDetails: Discount;
  originalAmount: number;
  discountAmount: number;
  totalAmount: number;
}
