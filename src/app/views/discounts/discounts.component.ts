import { Component, OnInit } from '@angular/core';
import { Discount } from 'src/app/models/Discount';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-discounts',
  templateUrl: './discounts.component.html',
  styleUrls: ['./discounts.component.css'],
})
export class DiscountsComponent implements OnInit {
  constructor(public st: StorageService) {}

  ngOnInit(): void {}

  addDiscount(discount: Discount) {
    this.st.storeDiscount(discount);
  }

  editDiscount(discounts: [Discount, Discount]) {
    const currentDiscount = discounts[0];
    const discountChanges = discounts[1];

    console.info('Updating Discount:', currentDiscount);
    this.st.editDiscount(currentDiscount, discountChanges);
  }

  deleteDiscount(discount: Discount) {
    console.info('Deleting discount', discount);
    this.st.deleteDiscount(discount);
  }
}
