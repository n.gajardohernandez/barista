import { Component } from '@angular/core';
import { Item } from 'src/app/models/Item';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
})
export class ItemsComponent {
  constructor(public st: StorageService, private sc: ShoppingCartService) {}

  editItem(items: [Item, Item]) {
    const currentItem = items[0];
    const changes = items[1];
    console.info('Updating item:', currentItem);

    this.st.editItem(currentItem, changes);
  }

  addItem(item: Item) {
    console.log('Adding item', item);
    this.st.storeItem(item);
  }

  deleteItem(item: Item) {
    console.info('Deleting item', item);
    this.st.deleteItem(item);
  }

  addToShoppingCart(item: Item) {
    console.log('Adding item to sc', item);
    this.sc.addItem({ ...item });
  }

  removeFromShoppingCart(item: Item) {
    console.log('Removing item from sc', item);
    this.sc.removeItem(item);
  }
}
