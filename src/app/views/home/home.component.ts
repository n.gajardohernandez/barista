import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Item } from 'src/app/models/Item';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(public st: StorageService, private sc: ShoppingCartService) {
    this.scItems$ = this.sc.shoppingCart$.pipe(map((sc) => sc.items));
  }

  scItems$: Observable<Item[]>;

  ngOnInit(): void {}

  addToShoppingCart(item: Item) {
    this.sc.addItem({ ...item });
  }

  removeFromShoppingCart(item: Item) {
    this.sc.removeItem(item);
  }
}
