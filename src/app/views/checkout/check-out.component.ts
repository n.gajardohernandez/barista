import { Component, OnDestroy, OnInit } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { map, takeUntil } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { Item } from 'src/app/models/Item';
import { CheckOutService } from 'src/app/services/check-out.service';
import { Constants } from 'src/app/utils/constants';
import { BillDiscountDetail } from 'src/app/models/bill-discount-detail';

@Component({
  selector: 'app-checkout',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css'],
})
export class CheckOutComponent implements OnInit, OnDestroy {
  constructor(
    public sc: ShoppingCartService,
    private checkout: CheckOutService
  ) {
    checkout.items$ = sc.shoppingCart$.pipe(
      takeUntil(this.notifier$),
      map((v) => v.items)
    );
  }

  displayedShoppingCartButtons = ['remove_from_sc'];

  message: string;

  notifier$ = new Subject();

  get shoppingCartitems$(): Observable<Item[]> {
    return this.checkout.items$;
  }

  get bill() {
    return this.checkout.bill;
  }

  private get bill$() {
    return this.checkout.calcBill$();
  }

  get billDiscountDetails$(): Observable<BillDiscountDetail[]> {
    return this.bill$.pipe(map((bill) => bill?.discounts));
  }

  ngOnInit(): void {
    this.message = '';
    this.bill$.subscribe((b) => console.log('bill: ', b));
  }

  ngOnDestroy(): void {
    this.notifier$.next();
    this.notifier$.complete();
  }

  removeFromShoppingCart(item: Item) {
    this.sc.removeItem(item);
  }

  completeOrder() {
    this.message = Constants.completeOrderMessage;
    this.displayedShoppingCartButtons = [''];
  }

  emptyCart() {
    this.sc.emptyCart();
    this.message = '';
    this.displayedShoppingCartButtons = ['remove_from_sc'];
  }
}
